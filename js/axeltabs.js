class AxelTabs {
    static init(selector, userOptions = {}) {
        let element = document.querySelector(selector);
        let controls = Array.from(element.querySelector('.controls').querySelectorAll('[class*="tab-"]'));
        let panels = Array.from(element.querySelector('.panels').querySelectorAll('[class*="tab-"]'));
        let options = Object.assign(Object.assign({}, this.defOptions), userOptions);
        let currentPanel = options.startAt;
        let tabs = {
            element: element,
            controls: controls,
            panels: panels,
            options: options,
            currentPanel: currentPanel,
            open: open
        };
        open(options.startAt);
        tabs.controls.forEach(el => {
            el.addEventListener('click', (ev => {
                open(el);
            }));
        });
        function open(indexOrElem) {
            let cls = null;
            if (typeof indexOrElem == 'number') {
                cls = 'tab-' + indexOrElem;
            }
            else {
                cls = Array.from(indexOrElem.classList.values()).find(cl => cl.includes('tab-'));
            }
            console.log('cls', tabs);
            tabs.panels.forEach(tab => {
                if (tab.classList.contains(cls))
                    tab.classList.add('active');
                else
                    tab.classList.remove('active');
            });
            tabs.controls.forEach(ctr => {
                if (ctr.classList.contains(cls))
                    ctr.classList.add('active');
                else
                    ctr.classList.remove('active');
            });
        }
        this.tabs.push(tabs) - 1;
        return tabs;
    }
    static open(panelIndex, indexOrName = 0) {
        let slider = this.getTabs(indexOrName);
        slider.open(panelIndex);
    }
    static getTabs(indexOrName) {
        if (typeof indexOrName === 'string') {
            return this.tabs.find((tabs) => tabs.options.name === indexOrName);
        }
        else {
            return this.tabs[indexOrName];
        }
    }
}
AxelTabs.tabs = [];
AxelTabs.defOptions = {
    name: null,
    startAt: 1
};
